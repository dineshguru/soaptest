<?php

class clsWSSEAuth
{
	private $Username;
	private $Password;

	function __construct($username, $password)
	{
		$this->Username = $username;
		$this->Password = $password;
	}
}

class clsWSSEToken
{
	private $UsernameToken;

	function __construct($innerVal)
	{
		$this->UsernameToken = $innerVal;
	}
}

class SoapWrite
{

	protected $client;

	protected $soapType = SOAP_LITERAL;

	protected $objSoapVarWSSEHeader;

	protected $WSDL = 'https://uatfxlink.filogix.com/fcxportal/services/FCX?WSDL';


	public function __construct($objSoapVarWSSEHeader = NULL, $wsdl = NULL)
	{
		if (!is_null($objSoapVarWSSEHeader))
		{
			$this->objSoapVarWSSEHeader = $objSoapVarWSSEHeader;
		}

		if (!is_null($wsdl))
		{
			$this->WSDL = $wsdl;
		}
	}

	public function write()
	{
		//set the soap client
		$this->setClient();

		$this->client->__setSoapHeaders([$this->objSoapVarWSSEHeader]);

		//var_dump($this->client->__getFunctions());

		//exit;

		//set soap Vars
		/*$data = $this->soapify([
			'Acquirer' => [
				'Id'       => 'MyId',
				'UserId'   => 'MyUserId',
				'Password' => 'MyPassword',
			],
		]);*/

		$data = $this->body();

		/*$this->display($data);

		exit;*/

		try
		{
			$method = 'setMessage';

			//request
			$response = $this->client->$method($data);

			//deal with response

		} catch (Exception $e)
		{
			var_dump($e->getMessage());
		}
	}

	public function setWsdl($wsdl)
	{
		if (strlen($wsdl) > 0)
		{
			$this->WSDL = $wsdl;
		}
	}

	public function getWsdl()
	{
		return $this->WSDL;
	}

	protected function setClient()
	{
		$this->client = new SoapClient(
			$this->WSDL,
			[
				'trace' => 1,
				'use'   => $this->soapType,
			]
		);
	}

	protected function body()
	{
		return <<<HERODC
		<setMessage xmlns="http://services.axis.fxportal.hermes.filogix.com">
			<message>
				<dealId>989898989898989898</dealId>
				<format>FCXAPI</format>
				<messageId>989898989898989898</messageId>
				<receiver>FILOGIX.EXPUAT.QA.1305</receiver>
				<sender> FILOGIX.EXPERTAPI.TEST</sender>
				<timestamp>2017-01-07T14:33:05</timestamp>
				<transaction>Referral Submission</transaction>
				<version>1.1.0</version>
				<documents>
					<item>
						<docname>Referral Application</docname>
						<format>FCXAPI</format>
						<mimetype>base64binary/xml</mimetype>
						<version>1.1.0</version>
						<content>Base64 encoded XML payload</content>
					</item>
				</documents>
			</message>
		</setMessage>

HERODC;

	}

	protected function soapify(array $data)
	{
		foreach ($data as &$value)
		{
			if (is_array($value))
			{
				$value = $this->soapify($value);
			}
		}

		return new SoapVar($data, SOAP_ENC_OBJECT);
	}

	protected function display($data)
	{
		if (is_array($data) OR is_object($data))
		{
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}
		else
		{
			print $data;
		}
	}
}

$username = 'plpops';
$password = 'p1p0p5';

//Check with your provider which security name-space they are using.
$strWSSENS = "http://schemas.xmlsoap.org/ws/2002/07/secext";

$objSoapVarUser = new SoapVar($username, XSD_STRING, NULL, $strWSSENS, NULL, $strWSSENS);
$objSoapVarPass = new SoapVar($password, XSD_STRING, NULL, $strWSSENS, NULL, $strWSSENS);

$objWSSEAuth = new clsWSSEAuth($objSoapVarUser, $objSoapVarPass);
$objSoapVarWSSEAuth = new SoapVar($objWSSEAuth, SOAP_ENC_OBJECT, NULL, $strWSSENS, 'UsernameToken', $strWSSENS);
$objWSSEToken = new clsWSSEToken($objSoapVarWSSEAuth);
$objSoapVarWSSEToken = new SoapVar($objWSSEToken, SOAP_ENC_OBJECT, NULL, $strWSSENS, 'UsernameToken', $strWSSENS);
$objSoapVarHeaderVal = new SoapVar($objSoapVarWSSEToken, SOAP_ENC_OBJECT, NULL, $strWSSENS, 'Security', $strWSSENS);
$objSoapVarWSSEHeader = new SoapHeader($strWSSENS, 'Security', $objSoapVarHeaderVal, TRUE, 'http://abce.com');

$d = new SoapWrite($objSoapVarWSSEHeader);
$d->write();

