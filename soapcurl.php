<?php
//Data, connection, auth
$dataFromTheForm = $_POST['fieldName']; // request data from the form
$soapUrl = "https://uatfxlink.filogix.com/fcxportal/services/FCX?WSDL"; // asmx URL of WSDL
$soapUser = "username";  //  username
$soapPassword = "password"; // password

// xml post structure

$xml_post_string = <<<HERODC
<?xml version="1.0" encoding="utf-8"?>
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Body>
		<setMessage xmlns="http://services.axis.fxportal.hermes.filogix.com">
			<message>
				<dealId>source application id</dealId>
				<format>FCXAPI</format>
				<messageId>1002</messageId>
				<receiver>FILOGIX.EXPUAT.QA.1305</receiver>
				<sender> FILOGIX.EXPERTAPI.TEST</sender>
				<timestamp>2017-01-07T14:33:05</timestamp>
				<transaction>Referral Submission</transaction>
				<version>1.1.0</version>
				<documents>
					<item>
						<docname>Referral Application</docname>
						<format>FCXAPI</format>
						<mimetype>base64binary/xml</mimetype>
						<version>1.1.0</version>
						<content>Base64 encoded XML payload</content>
					</item>
				</documents>
			</message>
		</setMessage>
	</soapenv:Body>
</soapenv:Envelope>

HERODC;

$headers = [
	"Content-type: text/xml;charset=\"utf-8\"",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://connecting.website.com/WSDL_Service/GetPrice",
	"Content-length: " . strlen($xml_post_string),
]; //SOAPAction: your op URL

$url = $soapUrl;

// PHP cURL  for https connection with auth
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// converting
$response = curl_exec($ch);
curl_close($ch);

// converting
$response1 = str_replace("<soap:Body>", "", $response);
$response2 = str_replace("</soap:Body>", "", $response1);

// convertingc to XML
$parser = simplexml_load_string($response2);
// user $parser to get your data out of XML response and to display it.
?>